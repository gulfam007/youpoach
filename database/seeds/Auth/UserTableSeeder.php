<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        User::create([
            'first_name'        => 'Admin',
            'last_name'         => 'Istrator',
            'email'             => config('youpoach.admin_email'),
            'password'          => bcrypt('1234'),
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        if (!App::environment('production')) {
            $faker = Faker\Factory::create();

            User::create([
                'first_name'        => 'Backend',
                'last_name'         => 'User',
                'email'             => 'executive@executive.com',
                'password'          => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
            ]);

            User::create([
                'first_name'        => 'Default',
                'last_name'         => 'User',
                'email'             => 'user@user.com',
                'password'          => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
            ]);

            User::create([
                'first_name'        => 'Default',
                'last_name'         => 'Employer',
                'email'             => 'user@company.com',
                'password'          => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
            ]);

            $statuses = ['Employed', 'Unemployed'];
            $jobs = ['Developer','Designer','Sales','HR'];
            $salaries = ['£100,000','£55,000','£65,000'];

            for ($i = 0; $i < 100; $i++) {
                User::create([
                    'first_name'        => $faker->firstName,
                    'last_name'         => $faker->lastName,
                    'email'             => $i.'@user.com',
                    'password'          => bcrypt('1234'),
                    'confirmation_code' => md5(uniqid(mt_rand(), true)),
                    'confirmed'         => true,
                    'current_status'    => $statuses[($i % count($statuses))],
                    'desired_roles'     => $jobs[($i % count($jobs))],
                    'desired_salary'    => $salaries[($i % count($salaries))],
                ]);
            }
        }

        $this->enableForeignKeys();
    }
}
