<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $executive = Role::create(['name' => 'executive']);
        $user = Role::create(['name' => config('access.users.default_role')]);
        $employer = Role::create(['name' => config('access.users.employer_role')]);

        // Create Permissions
        Permission::create(['name' => 'view backend']);
        Permission::create(['name' => 'view users']);

        // ALWAYS GIVE ADMIN ROLE ALL PERMISSIONS
        $admin->givePermissionTo('view backend');
        $admin->givePermissionTo('view users');

        // Executive Permissions
        $executive->givePermissionTo('view backend');

        // User Permissions
        // ...

        // Employer Permissions - Added when verified
        // $employer->givePermissionTo('view users');

        $this->enableForeignKeys();
    }
}
