<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        User::find(1)->assignRole(config('access.users.admin_role'));

        if (!App::environment('production')) {
            User::find(2)->assignRole('executive');
            User::find(3)->assignRole(config('access.users.default_role'));
            User::find(4)->assignRole(config('access.users.employer_role'));

            for ($i = 0; $i < 100; $i++) {
                User::find(5 + $i)->assignRole(config('access.users.default_role'));
            }
        }

        $this->enableForeignKeys();
    }
}
