<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'HomeController@index')->name('index');
Route::get('confirm', 'ConfirmController@index')->name('confirm');
Route::get('user-agreement', 'UserAgreementController@index')->name('user-agreement');
Route::get('security', 'SecurityController@index')->name('security');
Route::get('privacy', 'PrivacyController@index')->name('privacy');
Route::get('poaching-services', 'PoachingServicesController@index')->name('poaching-services');
Route::get('about', 'AboutController@index')->name('about');
Route::get('terms-and-conditions', 'TermsAndConditionsController@index')->name('terms-and-conditions');
Route::get('contact', 'ContactController@index')->name('contact');
Route::post('contact/send', 'ContactController@send')->name('contact.send');

/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    /**
     * Employer focused routes
     * Must have the 'view users' permission to access
     */
    Route::group(['namespace' => 'Employer', 'as' => 'employer.'], function () {
        // Employer verification page
        Route::get('verify', 'VerifyController@index')->name('verify');

        // Employer Profile Update
        Route::patch('employer/profile/update', 'ProfileController@update')->name('profile.update');

        Route::group(['middleware' => 'can:view users'], function () {
            // User Search
            Route::get('search', 'SearchController@index')->name('search');

            // User Viewing
            Route::get('user/{user}', 'UserController@index');

            // User Poaching
            Route::get('poach/{user}', 'PoachController@index');

            // Premium Accounts Sign Up
            Route::get('premium', 'PremiumController@index')->name('premium');
            Route::post('premium/subscribe', 'PremiumController@subscribe')->name('premium.subscribe');
            Route::post('premium/cancel', 'PremiumController@cancel')->name('premium.cancel');
            Route::post('premium/resume', 'PremiumController@resume')->name('premium.resume');
        });
    });

    /**
     * User focused routes
     */
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        //User Account Specific
        Route::get('account', 'AccountController@index')->name('account');

        // User Profile Specific
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    });
});
