![YouPoach](https://demo.itho.co/img/frontend/youpoach/logo-nav.png)

YouPoach is the new video poaching system that brings a dynamic and competitive edge to spotting talent.

## Installation
#### Server requirements
- PHP 7.1 (php7.1-curl, php7.1-mbstring, php7.1-dom, php7.1-mysql, php7.1-gd, unzip)

#### Project setup
```bash
# Clone the project
git clone git@bitbucket.org:sir-legendary/youpoach.git
cd youpoach

# Create the environment file
cp .env.example .env
```
Edit the following environment variables
- APP_ENV
- APP_DEBUG
- APP_URL
- DB_*
- MAIL_*
- GOOGLE_MAPS_API_KEY
- STRIPE_*
```bash
# Install Laravel project dependencies
composer install

# Create the encryption key
php artisan key:generate

# Run the database migrations
php artisan migrate

# Run the database seeds (needed for the first admin account)
php artisan db:seed
```

#### Login
After your project is installed and you can access it in a browser, click the login button on the right of the navigation bar.

The default administrator credentials are:

Username: admin@youpoach.com

Password: 1234

## Development
```bash
# Install JavaScript project dependencies
npm install

# Generate production files
npm run prod
```

## Testing
Running the tests will refresh the database using the migrations and seeds in the project.

```bash
./vendor/bin/phpunit
```
