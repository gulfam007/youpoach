<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin email address
    |--------------------------------------------------------------------------
    |
    | Used to generate the initial admin account
    |
    */

    'admin_email' => env('ADMIN_EMAIL', 'admin@youpoach.com'),

];
