<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\UserRepository;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;

/**
 * Class UserVerificationController.
 */
class UserVerificationController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function verify(User $user, ManageUserRequest $request)
    {
        $this->userRepository->verify($user);
        $user->givePermissionTo('view users');

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.verified'));
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function unverify(User $user, ManageUserRequest $request)
    {
        $this->userRepository->unverify($user);
        $user->revokePermissionTo('view users');

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.unverified'));
    }
}
