<?php

namespace App\Http\Controllers\Frontend;

use Auth;
use App\Http\Controllers\Controller;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (Auth::check()){
            return redirect()->route('frontend.user.dashboard');
        }

        return view('frontend.index');
    }
}
