<?php

namespace App\Http\Controllers\Frontend\Employer;

use App\Models\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Auth\UserRepository;

class PremiumController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('frontend.employer.premium');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subscribe(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if ($user->subscribed('main')) {
            return redirect()->back();
        }

        $plan = config('services.stripe.plan');
        $user->newSubscription('main', $plan)->create($request->stripeToken);

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancel(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (!$user->subscribed('main') || $user->subscription('main')->onGracePeriod()) {
            return redirect()->back();
        }

        $user->subscription('main')->cancel();

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function resume(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if (!$user->subscription('main')->onGracePeriod()) {
            return redirect()->back();
        }

        $user->subscription('main')->resume();

        return redirect()->back();
    }
}
