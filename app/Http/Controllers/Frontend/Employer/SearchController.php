<?php

namespace App\Http\Controllers\Frontend\Employer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Auth\UserRepository;
use App\Http\Requests\Frontend\Employer\SearchUserRequest;

class SearchController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(SearchUserRequest $request)
    {
        $keywords = $request->input('keywords');

        $users = $this->userRepository->search($keywords);

        if ($keywords != null && $users->total() == 0) {
            return redirect()->route('frontend.employer.search')->withFlashInfo('Unable to find any matches!');
        }

        return view('frontend.employer.search')->with(compact('users'));
    }
}
