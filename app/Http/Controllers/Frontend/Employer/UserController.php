<?php

namespace App\Http\Controllers\Frontend\Employer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Events\Frontend\Auth\UserViewed;
use App\Repositories\Frontend\Auth\UserRepository;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * ConfirmAccountController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $uuid)
    {
        $user = $this->user->findByUuid($uuid);

        // Only allow viewing of end users.
        // Redirect without prompt as not to confirm uuid's
        if ($user->isEndUser()) {
            event(new UserViewed($user));

            return view('frontend.employer.user-dashboard', compact('user'));
        } else {
            return redirect()->route('frontend.employer.search');
        }

    }
}
