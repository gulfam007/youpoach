<?php

namespace App\Http\Controllers\Frontend\Employer;

use App\Http\Controllers\Controller;

/**
 * Class VerifyController.
 */
class VerifyController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // If the reg_no is set then they must have submitted their details previously
        $submitted = (auth()->user()->registration_number !== null);

        return view('frontend.employer.verify')->with('submitted', $submitted);
    }
}
