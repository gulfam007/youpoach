<?php

namespace App\Http\Controllers\Frontend\Employer;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Auth\UserRepository;
use App\Http\Requests\Frontend\Employer\UpdateProfileRequest;

/**
 * Class ProfileController.
 */
class ProfileController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     */
    public function update(UpdateProfileRequest $request)
    {
        $output = $this->userRepository->updateEmployer(
            $request->user()->id,
            $request->only(
                'company_name',
                'registration_number',
                'location',
                'contact_name',
                'contact_position',
                'about',
                'poach_message',
                'avatar_location',
                'video_location'
            ),
            $request->has('avatar_location') ? $request->file('avatar_location') : false,
            $request->has('video_location') ? $request->file('video_location') : false
        );

        // E-mail address was updated, user has to reconfirm
        if (is_array($output) && $output['email_changed']) {
            auth()->logout();

            return redirect()->route('frontend.auth.login')->withFlashInfo(__('strings.frontend.user.email_changed_notice'));
        }

        return redirect()->route('frontend.index')->withFlashSuccess(__('strings.frontend.user.profile_updated'));
    }
}
