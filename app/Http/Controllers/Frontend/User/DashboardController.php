<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (auth()->user()->hasRole(config('access.users.employer_role')) && ! auth()->user()->verified) {
            return redirect()->route('frontend.employer.verify');
        }

        if (auth()->user()->hasRole(config('access.users.employer_role'))) {
            return view('frontend.employer.dashboard');
        }

        return view('frontend.user.dashboard');
    }
}
