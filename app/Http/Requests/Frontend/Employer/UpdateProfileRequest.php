<?php

namespace App\Http\Requests\Frontend\Employer;

use Illuminate\Validation\Rule;
use App\Helpers\Frontend\Auth\Socialite;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateProfileRequest.
 */
class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'  => 'required|max:191',
            'registration_number'  => 'required|max:191',
            'location'  => 'required|max:191',
            'contact_name'  => 'required|max:191',
            'contact_position'  => 'required|max:191',
            'about'  => 'required|max:2000',
            'poach_message' => 'sometimes|max:2000',
            'avatar_location' => 'sometimes|image|max:2000',
            'video_location' => 'sometimes|file|max:20000',
        ];
    }
}
