<?php

namespace App\Notifications\Frontend\Auth;

use App\Models\Auth\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class UserNeedsConfirmation.
 */
class UserPoachedNotification extends Notification
{
    use Queueable;

    /**
     * @var
     */
    protected $user;

    /**
     * UserNeedsConfirmation constructor.
     *
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (auth()->user()->poach_message) {
            $text = auth()->user()->poach_message;
        } else {
            $text = auth()->user()->company_name . ' (registration. no: '. auth()->user()->registration_number .')' . __('strings.emails.poach.text');
        }

        return (new MailMessage())
            ->subject(__('strings.emails.poach.subject'))
            ->line($text)
            // ->action(__('buttons.emails.poach.confirm_account'), route('frontend.auth.account.confirm', '123'))
            ->line(__('strings.emails.auth.thank_you_for_using_app'));
    }
}
