<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'delete_user_confirm'  => 'Are you sure you want to delete this user permanently? Anywhere in the application that references this user\'s id will most likely error. Proceed at your own risk. This can not be un-done.',
                'if_confirmed_off'     => '(If confirmed is off)',
                'no_deactivated'       => 'There are no deactivated users.',
                'no_deleted'           => 'There are no deleted users.',
                'no_unverified'        => 'There are no unverified users.',
                'restore_user_confirm' => 'Restore this user to its original state?',
            ],
        ],

        'dashboard' => [
            'title'   => 'Dashboard',
            'welcome' => 'Welcome',
        ],

        'general' => [
            'all_rights_reserved' => 'All Rights Reserved.',
            'are_you_sure'        => 'Are you sure you want to do this?',
            'app_name'            => 'YouPoach',
            'continue'            => 'Continue',
            'member_since'        => 'Member since',
            'minutes'             => ' minutes',
            'search_placeholder'  => 'Search...',
            'timeout'             => 'You were automatically logged out for security reasons since you had no activity in ',

            'see_all' => [
                'messages'      => 'See all messages',
                'notifications' => 'View all',
                'tasks'         => 'View all tasks',
            ],

            'status' => [
                'online'  => 'Online',
                'offline' => 'Offline',
            ],

            'you_have' => [
                'messages'      => '{0} You don\'t have messages|{1} You have 1 message|[2,Inf] You have :number messages',
                'notifications' => '{0} You don\'t have notifications|{1} You have 1 notification|[2,Inf] You have :number notifications',
                'tasks'         => '{0} You don\'t have tasks|{1} You have 1 task|[2,Inf] You have :number tasks',
            ],
        ],

        'search' => [
            'empty'      => 'Please enter a search term.',
            'incomplete' => 'You must write your own search logic for this system.',
            'title'      => 'Search Results',
            'results'    => 'Search Results for :query',
        ],

        'welcome' => '<p>This is the Admin Dashboard for <a href="https://youpoach.com/" target="_blank">YouPoach</a>.</p>
<p>Use the <strong>User Management</strong> to the left to manage users/roles/permissions.</p>
<p>Use the <strong>Log Viewer</strong> to the left to monitor system health. Please infrom your system administrator of any important messages.</p>
<p>- YouPoach</p>',
    ],

    'emails' => [
        'auth' => [
            'account_confirmed'       => 'Your account has been confirmed.',
            'error'                   => 'Whoops!',
            'greeting'                => 'Hello!',
            'regards'                 => 'Regards,',
            'trouble_clicking_button' => 'If you’re having trouble clicking the ":action_text" button, copy and paste the URL below into your web browser:',
            'thank_you_for_using_app' => 'Thank you for using our application!',

            'password_reset_subject'    => 'Reset Password',
            'password_cause_of_email'   => 'You are receiving this email because we received a password reset request for your account.',
            'password_if_not_requested' => 'If you did not request a password reset, no further action is required.',
            'reset_password'            => 'Click here to reset your password',

            'click_to_confirm' => 'Please verify your YouPoach account by clicking the link.',
        ],

        'contact' => [
            'email_body_title' => 'You have a new contact form request: Below are the details:',
            'subject'          => 'A new :app_name contact form submission!',
        ],

        'poach' => [
            'subject' => 'You\'ve been YouPoached!',
            'text'    => ' would like to get in contact with you!',
        ],

        'verify' => [
            'greeting' => 'Hi, :user_name',
        ],
    ],

    'frontend' => [
        'test' => 'Test',

        'tests' => [
            'based_on' => [
                'permission' => 'Permission Based - ',
                'role'       => 'Role Based - ',
            ],

            'js_injected_from_controller' => 'Javascript Injected from a Controller',

            'using_blade_extensions' => 'Using Blade Extensions',

            'using_access_helper' => [
                'array_permissions'     => 'Using Access Helper with Array of Permission Names or ID\'s where the user does have to possess all.',
                'array_permissions_not' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does not have to possess all.',
                'array_roles'           => 'Using Access Helper with Array of Role Names or ID\'s where the user does have to possess all.',
                'array_roles_not'       => 'Using Access Helper with Array of Role Names or ID\'s where the user does not have to possess all.',
                'permission_id'         => 'Using Access Helper with Permission ID',
                'permission_name'       => 'Using Access Helper with Permission Name',
                'role_id'               => 'Using Access Helper with Role ID',
                'role_name'             => 'Using Access Helper with Role Name',
            ],

            'view_console_it_works'          => 'View console, you should see \'it works!\' which is coming from FrontendController@index',
            'you_can_see_because'            => 'You can see this because you have the role of \':role\'!',
            'you_can_see_because_permission' => 'You can see this because you have the permission of \':permission\'!',
        ],

        'general' => [
            'joined' => 'Joined',
        ],

        'register' => [
            'register_user_header'    => 'Spot talent within minutes...',
            'register_user_subheader' => 'Express yourself',
            'register_employer_header' => 'You can only register with your company email address.',
            'panel_header'       => 'Be great at what you do',
            'panel_label'       => 'Get started - it\'s free',
            'radio_employee'     => 'Looking for an Opportunity',
            'radio_employer'     => 'Looking for Talent',
            'terms' => 'By creating an account, you agree to YouPoach\'s <a href=":terms_link">Terms of Service</a> and consent to our <a href=":security_link">Cookie Policy</a> and <a href=":privacy_link">Privacy Policy</a>.',
        ],

        'confirm' => [
            'heading'          => 'Almost there...',
            'subheading'       => 'You\'re one step away to be part of the community' ,
            'message_email'    => '<b>We Sent an email to :email</b><br>Click the link in the email message to verify email address...',
            'message_no_email' => '<b>We Sent an email to your email address</b><br>Click the link in the email message to verify email address...',
        ],

        'verify' => [
            'heading'          => 'Please',
            'subheading'       => 'You\'re one step away to be part of the community' ,
            'message_email'    => '<b>We Sent an email to :email</b><br>Click the link in the email message to verify email address...',
            'message_no_email' => '<b>We Sent an email to your email address</b><br>Click the link in the email message to verify email address...',
        ],

        'user_agreement' => [
            'heading' => 'User Agreement',
            'message' => '<p>Lorem ipsum dolor sit amet, tempor everti eleifend est ex. Facete splendide usu te, mea ad atqui antiopam vituperata, wisi errem mentitum ea sed. His id verterem constituto, meis idque possit ei duo, nostro postulant comprehensam usu ut. Mucius electram qui ad, duo erant legere no. Quis albucius apeirian ius et, movet referrentur liberavisse eum no. Erroribus constituam ut nam, probo veniam est ne, eam id ubique forensibus efficiantur.</p>
<p>An posidonium dissentias mea. Eos dico melius sadipscing id, cu elitr explicari persequeris mei. Nullam dolores referrentur sea eu, te qui aeterno nostrud incorrupte, et dicit inimicus principes ius. Te duo quis ancillae appetere, putant timeam oblique ex pri. Malis etiam noluisse his no.</p>
<p>Utinam scripserit adversarium an eos. Sea utinam audire petentium te, diceret nominati scripserit mei et. Verear eripuit mea et, ut simul equidem blandit vim. Te mel vitae laudem tacimates, in dicam postulant vituperatoribus mei. Cu fugit errem nominavi est, vel mundi malorum insolens id. Justo scripta mediocrem quo at, brute verear et mel, nam at sumo ferri.</p>
<p>Eu probo nominavi sed. Et est dicant ceteros oporteat, vim aliquip delenit pertinax an. In qui omnes facilisis erroribus, deleniti imperdiet honestatis usu cu. Alii quot prompta pri te. Eu vivendo mandamus his, eu labitur indoctum moderatius sit, atqui ancillae consectetuer cu sed.</p>
<p>Cu sed novum primis disputationi. Sea ex adhuc legendos vituperata, melius erroribus laboramus an est. Ne dicta vivendo mea, tritani forensibus at vel, sea no summo euismod. Dicta epicuri no nec.</p>',
        ],

        'security' => [
            'heading' => 'Security',
            'message' => '<p>Lorem ipsum dolor sit amet, tempor everti eleifend est ex. Facete splendide usu te, mea ad atqui antiopam vituperata, wisi errem mentitum ea sed. His id verterem constituto, meis idque possit ei duo, nostro postulant comprehensam usu ut. Mucius electram qui ad, duo erant legere no. Quis albucius apeirian ius et, movet referrentur liberavisse eum no. Erroribus constituam ut nam, probo veniam est ne, eam id ubique forensibus efficiantur.</p>
<p>An posidonium dissentias mea. Eos dico melius sadipscing id, cu elitr explicari persequeris mei. Nullam dolores referrentur sea eu, te qui aeterno nostrud incorrupte, et dicit inimicus principes ius. Te duo quis ancillae appetere, putant timeam oblique ex pri. Malis etiam noluisse his no.</p>
<p>Utinam scripserit adversarium an eos. Sea utinam audire petentium te, diceret nominati scripserit mei et. Verear eripuit mea et, ut simul equidem blandit vim. Te mel vitae laudem tacimates, in dicam postulant vituperatoribus mei. Cu fugit errem nominavi est, vel mundi malorum insolens id. Justo scripta mediocrem quo at, brute verear et mel, nam at sumo ferri.</p>
<p>Eu probo nominavi sed. Et est dicant ceteros oporteat, vim aliquip delenit pertinax an. In qui omnes facilisis erroribus, deleniti imperdiet honestatis usu cu. Alii quot prompta pri te. Eu vivendo mandamus his, eu labitur indoctum moderatius sit, atqui ancillae consectetuer cu sed.</p>
<p>Cu sed novum primis disputationi. Sea ex adhuc legendos vituperata, melius erroribus laboramus an est. Ne dicta vivendo mea, tritani forensibus at vel, sea no summo euismod. Dicta epicuri no nec.</p>',
        ],

        'privacy' => [
            'heading' => 'Privacy',
            'message' => '<p>Lorem ipsum dolor sit amet, tempor everti eleifend est ex. Facete splendide usu te, mea ad atqui antiopam vituperata, wisi errem mentitum ea sed. His id verterem constituto, meis idque possit ei duo, nostro postulant comprehensam usu ut. Mucius electram qui ad, duo erant legere no. Quis albucius apeirian ius et, movet referrentur liberavisse eum no. Erroribus constituam ut nam, probo veniam est ne, eam id ubique forensibus efficiantur.</p>
<p>An posidonium dissentias mea. Eos dico melius sadipscing id, cu elitr explicari persequeris mei. Nullam dolores referrentur sea eu, te qui aeterno nostrud incorrupte, et dicit inimicus principes ius. Te duo quis ancillae appetere, putant timeam oblique ex pri. Malis etiam noluisse his no.</p>
<p>Utinam scripserit adversarium an eos. Sea utinam audire petentium te, diceret nominati scripserit mei et. Verear eripuit mea et, ut simul equidem blandit vim. Te mel vitae laudem tacimates, in dicam postulant vituperatoribus mei. Cu fugit errem nominavi est, vel mundi malorum insolens id. Justo scripta mediocrem quo at, brute verear et mel, nam at sumo ferri.</p>
<p>Eu probo nominavi sed. Et est dicant ceteros oporteat, vim aliquip delenit pertinax an. In qui omnes facilisis erroribus, deleniti imperdiet honestatis usu cu. Alii quot prompta pri te. Eu vivendo mandamus his, eu labitur indoctum moderatius sit, atqui ancillae consectetuer cu sed.</p>
<p>Cu sed novum primis disputationi. Sea ex adhuc legendos vituperata, melius erroribus laboramus an est. Ne dicta vivendo mea, tritani forensibus at vel, sea no summo euismod. Dicta epicuri no nec.</p>',
        ],

        'terms_and_conditions' => [
            'heading' => 'Terms &amp; Conditions',
            'message' => '<p>Lorem ipsum dolor sit amet, tempor everti eleifend est ex. Facete splendide usu te, mea ad atqui antiopam vituperata, wisi errem mentitum ea sed. His id verterem constituto, meis idque possit ei duo, nostro postulant comprehensam usu ut. Mucius electram qui ad, duo erant legere no. Quis albucius apeirian ius et, movet referrentur liberavisse eum no. Erroribus constituam ut nam, probo veniam est ne, eam id ubique forensibus efficiantur.</p>
<p>An posidonium dissentias mea. Eos dico melius sadipscing id, cu elitr explicari persequeris mei. Nullam dolores referrentur sea eu, te qui aeterno nostrud incorrupte, et dicit inimicus principes ius. Te duo quis ancillae appetere, putant timeam oblique ex pri. Malis etiam noluisse his no.</p>
<p>Utinam scripserit adversarium an eos. Sea utinam audire petentium te, diceret nominati scripserit mei et. Verear eripuit mea et, ut simul equidem blandit vim. Te mel vitae laudem tacimates, in dicam postulant vituperatoribus mei. Cu fugit errem nominavi est, vel mundi malorum insolens id. Justo scripta mediocrem quo at, brute verear et mel, nam at sumo ferri.</p>
<p>Eu probo nominavi sed. Et est dicant ceteros oporteat, vim aliquip delenit pertinax an. In qui omnes facilisis erroribus, deleniti imperdiet honestatis usu cu. Alii quot prompta pri te. Eu vivendo mandamus his, eu labitur indoctum moderatius sit, atqui ancillae consectetuer cu sed.</p>
<p>Cu sed novum primis disputationi. Sea ex adhuc legendos vituperata, melius erroribus laboramus an est. Ne dicta vivendo mea, tritani forensibus at vel, sea no summo euismod. Dicta epicuri no nec.</p>',
        ],

        'user' => [
            'change_email_notice'  => 'If you change your e-mail you will be logged out until you confirm your new e-mail address.',
            'email_changed_notice' => 'You must confirm your new e-mail address before you can log in again.',
            'profile_updated'      => 'Profile successfully updated.',
            'password_updated'     => 'Password successfully updated.',
        ],

        'welcome_to' => 'Welcome to :place',
    ],
];
