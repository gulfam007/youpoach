<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'                 => 'Home',
        'logout'               => 'Logout',
        'verify'               => 'Verify',
        'user_agreement'       => 'User Agreement',
        'security'             => 'Security',
        'privacy'              => 'Privacy',
        'terms_and_conditions' => 'Terms &amp; Conditions',
    ],

    'frontend' => [
        'contact'           => 'Contact',
        'search'            => 'Search',
        'dashboard'         => 'Dashboard',
        'profile'           => 'Profile',
        'view_profile'      => 'View Profile',
        'login'             => 'Login',
        'macros'            => 'Macros',
        'register'          => 'Register',
        'rights_reserved'   => 'All Rights Reserved',
        'employer_services' => 'Poaching Services',
        'about'             => 'About',
        'advertise'         => 'Advertise',
        'sign_up'           => 'Sign Up',
        'careers'           => 'Careers',
        'blog'              => 'Blog',
        'press'             => 'Press',

        'user' => [
            'account'         => 'My Account',
            'settings'        => 'Settings',
            'account_edit'    => 'Edit Account',
            'administration'  => 'Administration',
            'change_password' => 'Change Password',
            'my_information'  => 'My Information',
            'profile'         => 'Profile',
        ],
        'employer' => [
            'premium'         => 'Premium'
        ],
    ],
];
