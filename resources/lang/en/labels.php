<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'       => 'All',
        'yes'       => 'Yes',
        'no'        => 'No',
        'not_applicable' => 'n/a',
        'copyright' => 'Copyright',
        'custom'    => 'Custom',
        'actions'   => 'Actions',
        'active'    => 'Active',
        'buttons' => [
            'save'   => 'Save',
            'update' => 'Update',
            'submit' => 'Submit',
        ],
        'hide'              => 'Hide',
        'inactive'          => 'Inactive',
        'none'              => 'None',
        'show'              => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions'     => 'Permissions',
                    'role'            => 'Role',
                    'sort'            => 'Sort',
                    'total'           => 'role total|roles total',
                ],
            ],

            'users' => [
                'active'              => 'Active Users',
                'all_permissions'     => 'All Permissions',
                'change_password'     => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create'              => 'Create User',
                'deactivated'         => 'Deactivated Users',
                'deleted'             => 'Deleted Users',
                'unverified'          => 'Unverified Users',
                'edit'                => 'Edit User',
                'management'          => 'User Management',
                'no_permissions'      => 'No Permissions',
                'no_roles'            => 'No Roles to set.',
                'permissions'         => 'Permissions',

                'table' => [
                    'confirmed'         => 'Email Confirmed',
                    'verified'          => 'Verified',
                    'created'           => 'Created',
                    'email'             => 'Email',
                    'id'                => 'ID',
                    'last_updated'      => 'Last Updated',
                    'name'              => 'Name',
                    'first_name'        => 'First Name',
                    'last_name'         => 'Last Name',
                    'no_deactivated'    => 'No Deactivated Users',
                    'no_deleted'        => 'No Deleted Users',
                    'other_permissions' => 'Other Permissions',
                    'permissions'       => 'Permissions',
                    'roles'             => 'Roles',
                    'social'            => 'Social',
                    'total'             => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmed',
                            'created_at'   => 'Created At',
                            'deleted_at'   => 'Deleted At',
                            'email'        => 'E-mail',
                            'last_updated' => 'Last Updated',
                            'name'         => 'Name',
                            'first_name'   => 'First name',
                            'last_name'    => 'Last name',
                            'status'       => 'Status',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],
    ],

    'frontend' => [

        'auth' => [
            'email'              => 'Email',
            'password'           => 'Password',
            'login_box_title'    => 'Login',
            'login_button'       => 'Login',
            'login_with'         => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button'    => 'Join Now',
            'remember_me'        => 'Remember Me',
        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button'    => 'Send Information',
        ],

        'passwords' => [
            'expired_password_box_title'      => 'Your password has expired.',
            'forgot_password'                 => 'Forgot Password?',
            'reset_password_box_title'        => 'Reset Password',
            'reset_password_button'           => 'Reset Password',
            'update_password_button'          => 'Update Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'user' => [
            'passwords' => [
                'change'   => 'Change Password',
            ],

            'profile' => [
                'details'            => 'Employee Details',
                'avatar'             => 'Avatar',
                'video'              => 'Video',
                'current_status'     => 'Current Status',
                'desired_roles'      => 'Desired Roles',
                'desired_salary'     => 'Salary Expectation',
                'available_start_date' => 'Available Start Date',
                'location'           => 'Location',
                'qualifications'     => 'Professional Qualifications',
                'created_at'         => 'Created At',
                'edit_information'   => 'Edit Information',
                'email'              => 'Email',
                'last_updated'       => 'Last Updated',
                'name'               => 'Name',
                'first_name'         => 'First name',
                'last_name'          => 'Last name',
                'update_information' => 'Update Information',
                'not_yet_set'        => 'Not yet set',
                'now'                => 'Now',
            ],
        ],

        'employer' => [
            'profile' => [
                'details'  => 'Company Details',
                'about'    => 'About',
                'location' => 'Locations',
            ],
        ],

        'search' => [
            'search' => 'Search',
            'title' => 'Search Talent',
            'keywords' => 'Keywords',
            'sectors' => 'Sectors',
            'location' => 'Location',
        ],

    ],
];
