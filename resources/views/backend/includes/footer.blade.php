<footer class="app-footer">
    <span class="float-right">
        <strong>{{ __('labels.general.copyright') }} &copy; {{ date('Y') }} <a href="https://youpoach.com">{{ __('strings.backend.general.app_name') }}</a></strong> {{ __('strings.backend.general.all_rights_reserved') }}
    </span>

    <!-- <span class="float-right">Powered by <a href="http://coreui.io">CoreUI</a></span> -->
</footer>