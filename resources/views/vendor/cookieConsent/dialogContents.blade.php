{{--<div class="js-cookie-consent cookie-consent">--}}
<div role="alert" class="alert alert-danger">
        <button type="button" data-dismiss="alert" aria-label="Close" class="close">
            <span aria-hidden="true">×</span>
        </button>

        {{--These credentials do not match our records.<br></div>--}}

    <span class="cookie-consent__message" style="align-content: center; text-align: center;">
        {!! trans('cookieConsent::texts.message') !!}
    </span>

    <button class="btn btn-success js-cookie-consent-agree cookie-consent__agree">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

</div>