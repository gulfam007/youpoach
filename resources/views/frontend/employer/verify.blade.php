@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.general.verify'))

@push('after-styles')
    <style>
        #app-wrapper {
            background: white;
        }
        .alert {
            margin-bottom: 6em;
        }
    </style>
@endpush

@push('app-toolbar')
    <div class="row" style="background: #393532; color: white; height: 100px;">
        <div class="col"></div>
    </div>
@endpush

@section('content')
    <div class="row">
        <div class="col">
            <div class="">
                <div class="">
                    <div class="row">
                        <div class="col col-sm-3 order-1 order-sm-1 mb-4" style="margin-top: -75px;">
                            <div class="card mb-4 bg-light">
                                <img class="card-img-top" src="{{ $logged_in_user->picture }}" alt="Profile Picture">

                                {{--<div class="card-body">--}}
                                    {{--<h4 class="card-title">--}}
                                        {{--{{ $logged_in_user->name }}<br/>--}}
                                    {{--</h4>--}}
                                {{--</div>--}}
                            </div>
                        </div><!--col-md-3-->

                        <div class="col-md-9 order-2 order-sm-2">
                            @if($logged_in_user->registration_number)
                                <div class="alert alert-dismissible alert-primary" style="margin-bottom: 1em;">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    Your details have been submitted and we are currently verifying your details, feel free to make any further changes!
                                </div>
                            @endif

                            <h3 class="mt-4">Company Details</h3>

                            {{ html()->modelForm($logged_in_user, 'PATCH', route('frontend.employer.profile.update'))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        {{ html()->label(__('validation.attributes.frontend.company_name'))->for('company_name') }}

                                        {{ html()->text('company_name')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.frontend.company_name'))
                                            ->attribute('maxlength', 191)
                                            ->required()
                                            ->autofocus() }}
                                    </div><!--form-group-->
                                </div><!--col-->
                                <div class="col">
                                    <div class="form-group">
                                        {{ html()->label(__('validation.attributes.frontend.registration_number'))->for('registration_number') }}

                                        {{ html()->text('registration_number')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.frontend.registration_number'))
                                            ->attribute('maxlength', 191)
                                            ->required()
                                            ->autofocus() }}
                                    </div><!--form-group-->
                                </div><!--col-->
                            </div><!--row-->

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        {{ html()->label(__('validation.attributes.frontend.company_logo'))->for('avatar_location') }}

                                        {{ html()->file('avatar_location')->class('form-control') }}
                                    </div><!--form-group-->
                                </div><!--col-->
                            </div><!--row-->

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        {{ html()->label(__('validation.attributes.frontend.location'))->for('location') }}

                                        {{ html()->text('location')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.frontend.location'))
                                            ->attribute('maxlength', 191)
                                            ->required()
                                            ->autofocus() }}
                                    </div><!--form-group-->
                                </div><!--col-->
                            </div><!--row-->

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        {{ html()->label(__('validation.attributes.frontend.contact_name'))->for('contact_name') }}

                                        {{ html()->text('contact_name')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.frontend.contact_name'))
                                            ->attribute('maxlength', 191)
                                            ->required()
                                            ->autofocus() }}
                                    </div><!--form-group-->
                                </div><!--col-->
                                <div class="col">
                                    <div class="form-group">
                                        {{ html()->label(__('validation.attributes.frontend.contact_position'))->for('contact_position') }}

                                        {{ html()->text('contact_position')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.frontend.contact_position'))
                                            ->attribute('maxlength', 191)
                                            ->required()
                                            ->autofocus() }}
                                    </div><!--form-group-->
                                </div><!--col-->
                            </div><!--row-->

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        {{ html()->label(__('validation.attributes.frontend.about'))->for('about') }}

                                        {{ html()->textarea('about')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.frontend.about'))
                                            ->attribute('maxlength', 2000)
                                            ->required()
                                            ->autofocus() }}
                                    </div><!--form-group-->
                                </div><!--col-->
                            </div><!--row-->

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0 clearfix">
                                        {{ form_submit(__('labels.general.buttons.submit')) }}
                                    </div><!--form-group-->
                                </div><!--col-->
                            </div><!--row-->
                            <br><br>
                            {{ html()->closeModelForm() }}
                        </div><!--col-md-9-->
                    </div><!-- row -->
                </div> <!-- card-body -->
            </div><!-- card -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
