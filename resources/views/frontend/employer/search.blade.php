@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.frontend.search'))

@push('after-styles')
    <style>
        #app-wrapper {
            background: white;
            padding-bottom: 25px;
        }
        .alert {
            margin-bottom: 6em;
        }
        .btn-brand {
            background: #f37721;
            color: white;
        }
        .quarter-width {
            min-width: 24%;
        }
        @media (min-width: 576px) {
            .shift-up-desktop {
                margin-top: -100px;
            }
        }
        @media (max-width: 575px) {
            .hide-mobile {
                display: none;
            }
            .btn-block-mobile {
                width: 100%;
                display: block;
            }
        }
    </style>
@endpush

@push('app-toolbar')
    <div style="background: #393532; color: white; min-height: 150px;">
        <div class="container">
            <div class="row">
                <div class="col mb-3" style="margin-top: 4rem;">
                    <h1 style="margin-bottom: 0px;">{{ __('labels.frontend.search.title') }}:</h1>

                    {{ html()->form('GET', route('frontend.employer.search'))->class('form-inline mt-3')->open() }}
                        {{ html()->text('keywords')
                                            ->class('form-control mr-sm-2 my-1 quarter-width')
                                            ->placeholder(__('labels.frontend.search.keywords'))
                                            ->attribute('maxlength', 191)
                                            ->required() }}

                        {{ html()->select('sector', ['Technology','Finance'])
                                            ->class('form-control mr-sm-2 my-1 quarter-width')
                                            ->placeholder(__('labels.frontend.search.sectors'))
                                            ->attribute('disabled', true) }}

                        {{ html()->select('location', ['UK','USA'])
                                            ->class('form-control mr-sm-2 my-1 quarter-width')
                                            ->placeholder(__('labels.frontend.search.location'))
                                            ->attribute('disabled', true) }}

                        <button class="btn btn-block-mobile btn-brand my-2 my-sm-0 quarter-width" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i> {{ __('labels.frontend.search.search') }}
                        </button>
                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
    </div>
@endpush

@section('content')
    <div class="row">
        <div class="col">
            <div class="col">
                @if (!isset($users) || $users->total() == 0)
                    <div class="col text-center mt-5">
                        Use the search tools above to discover the talent!
                    </div><!--col-md-4-->
                @else
                <div class="row">
                    @foreach ($users as $user)
                        <div class="col-xs-12 col-sm-3" style="padding: 5px; min-width: 33%;">
                        <div class="card" style="padding-left: 0px; padding-right: 0px;">
                            {{--<img class="card-img-top" src="{{ $user->avatar_location }}" alt="Card image cap">--}}
                            @if ($user->video_location)
                                <video height="auto" controls="controls" onclick="this.play()" class="card-img-top" style="width: 100%;">
                                    <source src="{{ $user->video_location }}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            @else
                                <img src="/img/frontend/video-placeholder.png" style="width: 100%;"/>
                            @endif
                            <div class="container-fluid">
                                <div class="row card-body" style="padding: 1rem; background: rgba(0,0,0,0.025);">
                                    <div class="col-xs-12 col-sm-7 px-0">
                                        <h5 class="card-title">{{ $user->first_name }} {{ $user->last_name }}</h5>
                                        <p class="card-text">{{ $user->desired_roles }}</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 px-0 text-right">
                                        <a href="/user/{{$user->uuid}}" class="btn btn-block-mobile btn-brand">View Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    @endforeach
                </div>

                <div style="float: right; padding-top: 15px;">
                    {!! $users->appends(Request::except('page'))->render('vendor.pagination.bootstrap-4') !!}
                </div>
                @endif
            </div><!-- row -->
        </div><!-- row -->
    </div><!-- row -->
    {{-- spacer --}}
    <div style="height: 350px"></div>
@endsection