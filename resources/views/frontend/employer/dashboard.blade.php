@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . $logged_in_user->company_name)

@push('after-styles')
    <style>
        #app-wrapper {
            background: white;
        }
        .alert {
            margin-bottom: 6em;
        }
        @media (min-width: 576px) {
            .shift-up-desktop {
                margin-top: -100px;
            }
        }
        @media (max-width: 575px) {
            .hide-mobile {
                display: none;
            }
        }
    </style>
@endpush

@push('app-toolbar')
    <div style="background: #393532; color: white; min-height: 150px;">
        <div class="container">
            <div class="row">
                <div class="col col-sm-3 order-1 order-sm-1 hide-mobile">&nbsp;</div>
                <div class="col col-md-9 order-2 order-sm-2 mb-3" style="margin-top: 4rem;">
                    <h1 style="margin-bottom: 0px;">{{ $logged_in_user->company_name }}</h1>
                    <small>Company registration number: {{ $logged_in_user->registration_number }}</small>
                </div>
            </div>
        </div>
    </div>
@endpush

@section('content')
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col col-sm-3 order-1 order-sm-1 mb-4 shift-up-desktop">
                    <div class="card mb-4 bg-light">
                        <img class="card-img-top" src="{{ $logged_in_user->picture }}" alt="Profile Picture">
                    </div>
                    <p class="card-text text-center">
                        <small>
                            <b>Contact Person:</b><br/>
                            {{ $logged_in_user->contact_name }}<br/>
                            ({{ $logged_in_user->contact_position }})<br/>
                        </small>
                    </p>
                </div><!--col-md-4-->

                <div class="col-md-9 order-2 order-sm-2">
                    {{--<h1>{{ $logged_in_user->company_name }}</h1>--}}

                    @if ($logged_in_user->hasVideo())
                        <video height="auto" controls="controls" onclick="this.play()" style="width: 100%;">
                            <source src="{{ $logged_in_user->video }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    @endif

                    <h3 class="mt-4">{{ __('labels.frontend.employer.profile.details') }}</h3>

                    <div class="mb-4">
                        <h5 class="mb-1">{{ __('labels.frontend.employer.profile.about') }}</h5>
                        @if($logged_in_user->about)
                            <p class="mb-1">{{ $logged_in_user->about }}</p>
                        @else
                            <p class="mb-1" style="color: grey;">{{ __('labels.frontend.employer.profile.not_yet_set') }}</p>
                        @endif
                    </div>

                    @if($logged_in_user->location)
                        <div class="mb-4">
                            <h5 class="mb-1">{{ __('labels.frontend.user.profile.location') }}</h5>
                            <iframe
                                width="100%"
                                height="350"
                                frameborder="0"
                                style="border: 0;"
                                src="https://www.google.com/maps/embed/v1/place?key={{ env('GOOGLE_MAPS_API_KEY') }}&q={{ $logged_in_user->location }}" allowfullscreen>
                            </iframe>
                        </div>
                    @endif
                </div><!--col-md-8-->
            </div><!-- row -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
