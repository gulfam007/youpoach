@extends('frontend.layouts.app')

@section('title', app_name() . ' | Premium Accounts')

@push('after-styles')
<style>
    #app {
        background-image: url("/img/frontend/youpoach/youpoach-bg-1920-1080.png");
    }

    .stripe-button-el,
    .stripe-button-el span {
        background: #f37721 !important;
        border-radius: 3px;
    }
</style>
@endpush

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div id="confirm-heading">YouPoach Premium</div>
                            <div id="confirm-subheading">Connect with talent more efficiently!</div>
                            <div id="confirm-message">
                                By signing up with us you can customise the interaction you have with your potential talent.
                                <br><br>
                                Only $9.99 per month!
                            </div>
                            <br>

                            @if ($logged_in_user->isSubscribed() && !$logged_in_user->isOnGracePeriod())
                                <div>
                                    You are already subscribed! :)<br>
                                    Make sure to edit your poaching messages in your <a href="{{ route('frontend.user.account') }}">account settings</a>!
                                </div>
                            @elseif ($logged_in_user->isSubscribed() && $logged_in_user->isOnGracePeriod())
                                <div>Your plan will end at the end of this billing cycle :(</div>
                            @else
                                <div></div>
                            @endif
                            <br>


                            @if ($logged_in_user->isSubscribed() && !$logged_in_user->isOnGracePeriod())
                                <form action="/premium/cancel" method="POST">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger">Unsubscribe</button>
                                </form>
                            @elseif ($logged_in_user->isSubscribed() && $logged_in_user->isOnGracePeriod())
                                <form action="/premium/resume" method="POST">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-success">Resume</button>
                                </form>
                            @else
                                <form action="/premium/subscribe" method="POST">
                                    {{ csrf_field() }}
                                    <script
                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="{{ env('STRIPE_KEY') }}"
                                            data-amount="999"
                                            data-name="YouPoach Premium"
                                            data-description="Online video poaching system"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto"
                                            data-currency="usd">
                                    </script>
                                </form>
                            @endif

                        </div>
                        <div class="col-md-5 text-sm-center">
                            <img id="confirm-image" src="{{ URL::asset('/img/frontend/premium.png') }}" alt="Verify">
                        </div>
                    </div>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
    {{-- spacer --}}
    <div style="height: 250px"></div>
@endsection
