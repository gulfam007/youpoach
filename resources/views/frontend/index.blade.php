@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.general.home'))

@push('after-styles')
<style>
    #app {
        background-image: url("/img/frontend/youpoach/youpoach-bg-1920-1080.png");
    }
    @media (min-width: 475px) {
        #register-header {
            padding-top: 50px;
        }
    }
    #register-user-header, #register-employer-header {
        color: white;
        font-size: 32pt;
    }
    #register-user-subheader, #register-employer-subheader {
        color: white;
        font-size: 32pt;
        font-weight: 600;
        text-transform: uppercase;
    }
    #register-arrow {
        text-align: right;
        margin-right: -20px;
    }
    @media (max-width: 474px) {
        #register-arrow {
            display: none;
        }
    }
</style>
@endpush

@section('content')
    <div class="row mb-4">
        <div class="col-xs-12 col-lg-4">
            {{--<div id="register-user-header">{{ __('strings.frontend.register.register_user_header') }}</div>--}}
            {{--<div id="register-user-subheader">{{ __('strings.frontend.register.register_user_subheader') }}</div>--}}
            {{--<div id="register-employer-header" style="display: none;">{{ __('strings.frontend.register.register_employer_header') }}</div>--}}
            {{--<div id="register-arrow" class="hidden-md-down">--}}
                {{--<img src="/img/frontend/youpoach/register-arrow-white.png" width="80%"/>--}}
            {{--</div>--}}
        </div>
        <div class="col-xs-12 col-lg-5">
            <div class="card">
                <h4 class="text-center" style="text-transform: uppercase;margin: 20px 0 10px;">
                    {{ __('strings.frontend.register.panel_header') }}
                </h4>
                <h6 class="text-center" >
                    {{ __('strings.frontend.register.panel_label') }}
                </h6>
                <div class="card-body" style="background: #EEE;border-top: 3px solid lightgray">
                    {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}
                    <fieldset>
                        <div class="" style="color: #000">
                            {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}

                            {{ html()->text('first_name')
                                ->class('form-control')
                                ->attribute('maxlength', 191) }}
                        </div>

                        <div class="" style="color:  #000">
                            {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}

                            {{ html()->text('last_name')
                                ->class('form-control')
                                ->attribute('maxlength', 191) }}
                        </div>

                        <div class="" style="color: #000">
                            {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                            {{ html()->email('email')
                                ->class('form-control')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--form-group-->

                        <div class="" style="color:  #000">
                            {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}
                            {{ html()->label(__('validation.attributes.frontend.more'))->for('more')->style("font-size:12px") }}

                            {{ html()->password('password')
                                ->class('form-control')
                                ->required() }}
                        </div><!--form-group-->

                        {{--<div class="form-group">--}}
                            {{--{{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}--}}

                            {{--{{ html()->password('password_confirmation')--}}
                                {{--->class('form-control')--}}
                                {{--->required() }}--}}
                        {{--</div><!--form-group-->--}}

                        @if (config('access.captcha.registration'))
                            <div class="row">
                                <div class="col">
                                    {!! Captcha::display() !!}
                                    {{ html()->hidden('captcha_status', 'true') }}
                                </div><!--col-->
                            </div><!--row-->
                        @endif

                        <fieldset class="form-group text-center">
                            <div class="form-check" style="display: inline-block; padding-end: 15px;color: #000">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="account_type" id="optionsRadios2" value="user" checked="">
                                    {{ __('strings.frontend.register.radio_employee') }}
                                </label>
                            </div>
                            <div class="form-check" style="display: inline-block;color: #000">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="account_type" id="optionsRadios1" value="employer">
                                    {{ __('strings.frontend.register.radio_employer') }}
                                </label>
                            </div>
                        </fieldset>

                        <div class="form-group">
                            <small id="fileHelp" class="form-text text-muted">
                                {!! __('strings.frontend.register.terms', [
                                    'terms_link' => route('frontend.terms-and-conditions'),
                                    'security_link' => route('frontend.security'),
                                    'privacy_link' => route('frontend.privacy')
                                ]) !!}
                            </small>
                        </div>

                        <div class="form-group mb-0 clearfix">
                            <button class="btn btn-primary btn-block" type="submit" style="font-weight: bold">
                                {{__('labels.frontend.auth.register_button')}}
                            </button>
                        </div><!--form-group-->

                    </fieldset>
                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('after-scripts')
<script>
    $("input[name='account_type']").change(function(){
        if ($("input[name='account_type']:checked").val() === 'user') {
            $("#register-user-header").show();
            $("#register-user-subheader").show();
            $("#register-employer-header").hide();
        } else if ($("input[name='account_type']:checked").val() === 'employer') {
            $("#register-user-header").hide();
            $("#register-user-subheader").hide();
            $("#register-employer-header").show();
        } else {
            // ...
        }
    });
</script>
@endpush