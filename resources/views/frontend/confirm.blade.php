@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.general.verify'))

@push('after-styles')
<style>
    #app {
        background-image: url("/img/frontend/youpoach/youpoach-bg-1920-1080.png");
    }
</style>
@endpush

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div id="confirm-heading">{{ __('strings.frontend.confirm.heading') }}</div>
                            <div id="confirm-subheading">{{ __('strings.frontend.confirm.subheading') }}</div>
                            @if (session('email'))
                                <div id="confirm-message">{!! __('strings.frontend.confirm.message_email', ['email' => session('email')]) !!}</div>
                            @else
                                <div id="confirm-message">{!! __('strings.frontend.confirm.message_no_email') !!}</div>
                            @endif
                        </div>
                        <div class="col-md-5 text-sm-center">
                            <img id="confirm-image" src="{{ URL::asset('/img/frontend/confirm.png') }}" alt="Verify">
                        </div>
                    </div>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
    {{-- spacer --}}
    <div style="height: 250px"></div>
@endsection
