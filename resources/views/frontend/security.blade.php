@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.general.security'))

@push('after-styles')
<style>
    #app {
        background-image: url("/img/frontend/youpoach/youpoach-bg-1920-1080.png");
    }
</style>
@endpush

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div id="verify-heading">{!! __('strings.frontend.security.heading') !!}</div>
                    <div id="verify-message">{!! __('strings.frontend.security.message') !!}</div>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
    {{-- spacer --}}
    <div style="height: 250px"></div>
@endsection
