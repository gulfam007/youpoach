@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . $logged_in_user->name)

@push('after-styles')
    <style>
        #app-wrapper {
            background: white;
        }
        .alert {
            margin-bottom: 6em;
        }
    </style>
@endpush

@push('app-toolbar')
    <div class="row" style="background: #393532; color: white; height: 100px;">
        <div class="col"></div>
    </div>
@endpush

@section('content')
    <div class="row">
        <div class="col">
            <div class="">
                <div class="">
                    <div class="row">
                        <div class="col col-sm-3 order-1 order-sm-1 mb-4" style="margin-top: -75px;">
                            <div class="card mb-4 bg-light">
                                <img class="card-img-top" src="{{ $logged_in_user->picture }}" alt="Profile Picture">

                                <div class="card-body">
                                    <h4 class="card-title">
                                        {{ $logged_in_user->name }}<br/>
                                    </h4>

                                    <p class="card-text">
                                        <small>
                                            <i class="fa fa-envelope-o"></i> {{ $logged_in_user->email }}<br/>
                                            <i class="fa fa-calendar-check-o"></i> {{ __('strings.frontend.general.joined') }} {{ $logged_in_user->created_at->timezone(get_user_timezone())->format('F jS, Y') }}
                                        </small>
                                    </p>

                                    <p class="card-text">
                                        <a href="{{ route('frontend.user.account')}}" class="btn btn-info btn-sm btn-block mb-1">
                                            <i class="fa fa-user-circle-o"></i> {{ __('navs.frontend.user.account_edit') }}
                                        </a>

                                        @can('view backend')
                                            <a href="{{ route ('admin.dashboard')}}" class="btn btn-danger btn-sm btn-block mb-1">
                                                <i class="fa fa-user-secret"></i> {{ __('navs.frontend.user.administration') }}
                                            </a>
                                        @endcan
                                    </p>
                                </div>
                            </div>
                        </div><!--col-md-4-->

                        <div class="col-md-9 order-2 order-sm-2">
                            @if ($logged_in_user->hasVideo())
                                <video height="auto" controls="controls" onclick="this.play()" style="width: 100%;">
                                    <source src="{{ $logged_in_user->video }}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            @else
                                <img src="/img/frontend/video-placeholder.png" style="width: 100%;"/>
                            @endif

                            <h3 class="mt-4">{{ __('labels.frontend.user.profile.details') }}</h3>

                            <div class="mb-4">
                                <h5 class="mb-1">{{ __('labels.frontend.user.profile.current_status') }}</h5>
                                @if($logged_in_user->current_status)
                                    <p class="mb-1">{{ $logged_in_user->current_status }}</p>
                                @else
                                    <p class="mb-1" style="color: grey;">{{ __('labels.frontend.user.profile.not_yet_set') }}</p>
                                @endif
                            </div>

                            <div class="mb-4">
                                <h5 class="mb-1">{{ __('labels.frontend.user.profile.desired_roles') }}</h5>
                                @if($logged_in_user->desired_roles)
                                    <p class="mb-1">{{ $logged_in_user->desired_roles }}</p>
                                @else
                                    <p class="mb-1" style="color: grey;">{{ __('labels.frontend.user.profile.not_yet_set') }}</p>
                                @endif
                            </div>

                            <div class="mb-4">
                                <h5 class="mb-1">{{ __('labels.frontend.user.profile.desired_salary') }}</h5>
                                @if($logged_in_user->desired_salary)
                                    <p class="mb-1">{{ $logged_in_user->desired_salary }}</p>
                                @else
                                    <p class="mb-1" style="color: grey;">{{ __('labels.frontend.user.profile.not_yet_set') }}</p>
                                @endif
                            </div>

                            @if($logged_in_user->location)
                                <div class="mb-4">
                                    <h5 class="mb-1">{{ __('labels.frontend.user.profile.location') }}</h5>
                                    <iframe
                                        width="100%"
                                        height="350"
                                        frameborder="0"
                                        style="border: 0;"
                                        src="https://www.google.com/maps/embed/v1/place?key={{ env('GOOGLE_MAPS_API_KEY') }}&q={{ $logged_in_user->location }}" allowfullscreen>
                                    </iframe>
                                </div>
                            @endif

                            <div class="mb-4">
                                <h5 class="mb-1">{{ __('labels.frontend.user.profile.available_start_date') }}</h5>
                                @if($logged_in_user->available_start_date)
                                    <p class="mb-1">{{ $logged_in_user->available_start_date }}</p>
                                @else
                                    <p class="mb-1">{{ __('labels.frontend.user.profile.now') }}</p>
                                @endif
                            </div>

                            <div class="mb-4">
                                <h5 class="mb-1">{{ __('labels.frontend.user.profile.qualifications') }}</h5>
                                @if($logged_in_user->qualifications)
                                    <p class="mb-1">{{ $logged_in_user->qualifications }}</p>
                                @else
                                    <p class="mb-1" style="color: grey;">{{ __('labels.frontend.user.profile.not_yet_set') }}</p>
                                @endif
                            </div>
                        </div><!--col-md-8-->
                    </div><!-- row -->
                </div> <!-- card-body -->
            </div><!-- card -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
