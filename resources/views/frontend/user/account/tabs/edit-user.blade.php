<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}

            {{ html()->text('first_name')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.first_name'))
                ->attribute('maxlength', 191)
                ->required()
                ->autofocus() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}

            {{ html()->text('last_name')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.last_name'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

@if ($logged_in_user->canChangeEmail())
    <div class="row">
        <div class="col">
            <div class="alert alert-info">
                <i class="fa fa-info-circle"></i> {{  __('strings.frontend.user.change_email_notice') }}
            </div>

            <div class="form-group">
                {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                {{ html()->email('email')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.email'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
@endif

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.current_status'))->for('current_status') }}

            {{ html()->text('current_status')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.current_status'))
                ->attribute('maxlength', 191) }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.desired_roles'))->for('desired_roles') }}

            {{ html()->text('desired_roles')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.desired_roles'))
                ->attribute('maxlength', 191) }}
            <small>{{ __('validation.attributes.frontend.comma_separate') }}</small>
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.desired_salary'))->for('desired_salary') }}

            {{ html()->text('desired_salary')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.desired_salary'))
                ->attribute('maxlength', 191) }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.available_start_date'))->for('available_start_date') }}

            {{ html()->text('available_start_date')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.available_start_date'))
                ->attribute('maxlength', 191) }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.location'))->for('location') }}

            {{ html()->text('location')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.location'))
                ->attribute('maxlength', 191) }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.qualifications'))->for('qualifications') }}

            {{ html()->text('qualifications')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.qualifications'))
                ->attribute('maxlength', 191) }}
            <small>{{ __('validation.attributes.frontend.comma_separate') }}</small>
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->