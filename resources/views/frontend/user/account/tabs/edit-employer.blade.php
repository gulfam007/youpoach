<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.company_name'))->for('company_name') }}

            {{ html()->text('company_name')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.company_name'))
                ->attribute('maxlength', 191)
                ->required()
                ->autofocus() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.registration_number'))->for('registration_number') }}

            {{ html()->text('registration_number')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.registration_number'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.location'))->for('location') }}

            {{ html()->text('location')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.location'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.contact_name'))->for('contact_name') }}

            {{ html()->text('contact_name')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.contact_name'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.contact_position'))->for('contact_position') }}

            {{ html()->text('contact_position')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.contact_position'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.about'))->for('about') }}

            {{ html()->textarea('about')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.about'))
                ->attribute('maxlength', 2000)
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

@if ($logged_in_user->isSubscribed() && !$logged_in_user->isOnGracePeriod())
    <div class="row">
        <div class="col">
            <div class="form-group">
                {{ html()->label(__('validation.attributes.frontend.poach_message'))->for('poach_message') }}

                {{ html()->textarea('poach_message')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.poach_message'))
                    ->attribute('maxlength', 2000) }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
@endif