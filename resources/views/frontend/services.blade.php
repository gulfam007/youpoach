@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.frontend.employer_services'))

@push('after-styles')
    <style>
        #app-wrapper {
            background: white;
        }
        .highlight {
            color: #f37721 !important;
        }
    </style>
@endpush

{{--@push('app-toolbar')--}}
    {{--<div class="row" style="background: #393532; color: white; text-align: center; padding-top: 2em; padding-bottom: 2em; margin-left: 0px; margin-right: 0px;">--}}
        {{--<div class="col">--}}
            {{--<h1>Spot talent within minutes; express yourself</h1>--}}
            {{--<p>YouPoach is the new millennial video poaching system that brings a dynamic and competitve edge to spotting talent.</p>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endpush--}}

@section('content')
    <div class="row mb-4">
        <div class="col">
            <h2 class="highlight" style="text-align: center;">Our Services to YouPoachers</h2>
        </div><!--col-->
    </div><!--row-->
@endsection
