<!--Footer-->
<footer id="app-footer">
    <!--Footer Links-->
    <div class="container-fluid brand-fade">
        <div class="row text-center footer-main">
            <!--First column-->
            <div class="col-md-6 my-auto">
                <a href="{{ route('frontend.index') }}">
                    <img id="footer-logo" src="/img/frontend/youpoach/logo-white.png" alt="{{ app_name() }}"/>
                </a>
                <p style="font-size: 0.75rem; padding-top: 10px;">&copy; {{ date('Y') }} {{ app_name() }} | {{ __('navs.frontend.rights_reserved') }}</p>
            </div>
            <!--/.First column-->

            <!--Second column-->
            <div class="col-md-6 footer-links my-auto">
                <div>
                    <ul>
                        <li><a href="{{route('frontend.poaching-services')}}">{{ __('navs.frontend.employer_services') }}</a></li>
                        <li><a href="{{route('frontend.about')}}">{{ __('navs.frontend.about') }}</li>
                        <li><a href="{{route('frontend.index')}}">{{ __('navs.frontend.advertise') }}</a></li>
                        <li><a href="{{route('frontend.contact')}}">{{ __('navs.frontend.contact') }}</a></li>
                    </ul>
                    <hr class="my-1 footer-divider">
                    <ul>
                        <li><a href="{{route('frontend.index')}}">{{ app_name() }}</a></li>
                        <li><a href="{{route('frontend.index')}}">{{ __('navs.frontend.sign_up') }}</a></li>
                        <li><a href="{{route('frontend.index')}}">{{ __('navs.frontend.careers') }}</a></li>
                        <li><a href="{{route('frontend.index')}}">{{ __('navs.frontend.blog') }}</a></li>
                        <li><a href="{{route('frontend.index')}}">{{ __('navs.frontend.press') }}</a></li>
                    </ul>
                </div>
            </div>
            <!--/.Second column-->
        </div>
    </div>
    <!--/.Footer Links-->

    <!--Legal-->
    <div class="container-fluid footer-legal">
        <ul>
            <li><a href="{{route('frontend.user-agreement')}}">{{ __('navs.general.user_agreement') }}</a></li>
            <li><a href="{{route('frontend.security')}}">{{ __('navs.general.security') }}</a></li>
            <li><a href="{{route('frontend.privacy')}}">{{ __('navs.general.privacy') }}</a></li>
            <li><a href="{{route('frontend.terms-and-conditions')}}">{{ __('navs.general.terms_and_conditions') }}</a></li>
        </ul>
    </div>
    <!--/.Legal-->
</footer>
<!--/.Footer-->