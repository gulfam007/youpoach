<nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding: 0;">
    <a href="{{ route('frontend.index') }}" class="navbar-brand">
        <img src="/img/frontend/youpoach/logo-nav.png" alt="{{ app_name() }}"/>
    </a>

    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('labels.general.toggle_navigation') }}">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav">
            @auth
                @can('view users')
                    <li class="nav-item">
                        <a href="{{route('frontend.employer.search')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.employer.search')) }}">
                            {{ __('navs.frontend.search') }}
                        </a>
                    </li>
                @endcan

                {{--<li class="nav-item">--}}
                    {{--<a href="{{route('frontend.user.dashboard')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}">--}}
                        {{--{{ __('navs.frontend.profile') }}--}}
                    {{--</a>--}}
                {{--</li>--}}
            @endauth

            {{--@if (config('locale.status') && count(config('locale.languages')) > 1)--}}
                {{--<li class="nav-item dropdown">--}}
                    {{--<a href="#" class="nav-link dropdown-toggle" id="navbarDropdownLanguageLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                        {{--{{ __('menus.language-picker.language') }} ({{ strtoupper(app()->getLocale()) }})--}}
                    {{--</a>--}}

                    {{--@include('includes.partials.lang')--}}
                {{--</li>--}}
            {{--@endif--}}

            @guest
                {{ html()->form('POST', route('frontend.auth.login.post'))->class('form-inline mx-2 my-2 my-lg-0')->open() }}
                    {{ html()->email('email')
                                        ->class('form-control mr-sm-2')
                                        ->placeholder(__('labels.frontend.auth.email'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}

                    {{ html()->password('password')
                                        ->class('form-control mr-sm-2')
                                        ->placeholder(__('labels.frontend.auth.password'))
                                        ->required() }}

                    <button class="btn btn-secondary my-2 my-sm-0" type="submit">
                        <i class="fa fa-lock" aria-hidden="true"></i> {{ __('labels.frontend.auth.login_button') }}
                    </button>
                {{ html()->form()->close() }}

                <li class="nav-item">
                    <a href="{{ route('frontend.auth.password.reset') }}" class="nav-link {{ active_class(Active::checkRoute('frontend.auth.password.reset')) }}">
                        {{ __('labels.frontend.passwords.forgot_password') }}
                    </a>
                </li>
            @else
                <li id="nav-user" class="nav-item dropdown hidden-md-up">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if($logged_in_user->picture)
                            <img style="width: 24px; height: 24px; border-radius: 50%; vertical-align: top; margin-right: 5px;" src="{{ URL::to($logged_in_user->picture) }}"></img> @if($logged_in_user->company_name){{ $logged_in_user->company_name }}@else{{ $logged_in_user->name }}@endif
                        @else
                            <i class="fa fa-user-circle-o mx-2"></i> @if($logged_in_user->company_name){{ $logged_in_user->company_name }}@else{{ $logged_in_user->name }}@endif
                        @endif
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuUser">
                        @if($logged_in_user->picture)
                            <a href="{{route('frontend.user.dashboard')}}" style="display: flex;" class="dropdown-item {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}">
                                <div>
                                    <img style="width: 50px; height: 50px; border-radius: 50%; vertical-align: top; margin-right: 10px;" src="{{ URL::to($logged_in_user->picture) }}"></img>
                                </div>
                                <div>
                                    {{ $logged_in_user->name }}<br>
                                    {{ __('navs.frontend.view_profile') }}
                                </div>
                            </a>
                        @else
                            <a href="{{route('frontend.user.dashboard')}}" class="dropdown-item {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}">
                                {{ __('navs.frontend.view_profile') }}
                            </a>
                        @endif

                        @can('view backend')
                            <a href="{{ route('admin.dashboard') }}" class="dropdown-item">
                                {{ __('navs.frontend.user.administration') }}
                            </a>
                        @endcan

                        <a href="{{ route('frontend.user.account') }}" class="dropdown-item {{ active_class(Active::checkRoute('frontend.user.account')) }}">
                            {{ __('navs.frontend.user.settings') }}
                        </a>

                        @if (config('locale.status') && count(config('locale.languages')) > 1)
                            <a href="#" class="dropdown-item dropdown-toggle" id="navbarDropdownLanguageLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('menus.language-picker.language') }} ({{ strtoupper(app()->getLocale()) }})
                            </a>

                            @include('includes.partials.lang')
                        @endif

                        @can('view users')
                            <a href="{{ route('frontend.employer.premium') }}" class="dropdown-item {{ active_class(Active::checkRoute('frontend.employer.premium')) }}">
                                {{ __('navs.frontend.employer.premium') }}
                            </a>
                        @endcan
                        <a href="{{ route('frontend.auth.logout') }}" class="dropdown-item">
                            {{ __('navs.general.logout') }}
                        </a>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</nav>
