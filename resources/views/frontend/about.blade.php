@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.frontend.employer_services'))

@push('after-styles')
    <style>
        #app-wrapper {
            background: white;
        }
        .highlight {
            color: #f37721 !important;
        }
    </style>
@endpush

@push('app-toolbar')
    <div class="row" style="background: #393532; color: white; text-align: center; padding-top: 2em; padding-bottom: 2em; margin-left: 0px; margin-right: 0px;">
        <div class="col">
            <h1>Spot talent within minutes; express yourself</h1>
            <p>YouPoach is the new millennial video poaching system that brings a dynamic and competitve edge to spotting talent.</p>
        </div>
    </div>
@endpush

@section('content')
    <div class="row mb-4">
        <div class="col">
            <h2 class="highlight" style="text-align: center;">Why YouPoach?</h2>
            <br>
            <h3>TALENT</h3>
            <p>YouPoach provides a platform for talent to express themselves professionally by producing a 60 second
                video pitch and creating a profile. Let the opportunity find you...</p>
            <h3>See how it works...</h3>
            <img width="100%" style="padding-top: 15px; padding-bottom: 15px;" src="{{ URL::asset('/img/frontend/about-0.png') }}"/>
            <h3 class="highlight">Get poached by employers.</h3>
            <p>Once you create your profile, it will be accessible or employers to contact you through our internal messaging
                system and discuss opportunities that may be suitable to you. Instead of you struggling to find your next
                opportunity, YouPoach creates a platform for employers to head hunt you.</p>
            <p>YouPoach is not a recruitment agency but operates as a server that brings employers and talent together.</p>
            <br>
            <h3>POACHER</h3>
            <p>Poachers are able to target, filter, shortlist and poach ethically to secure talent from a wide range of professionals;
                within minutes.</p>
            <p>YouPoach strongly values diversity welcoming all talents to express themselves.</p>
            <h3>See how it works...</h3>
            <img width="100%" style="padding-top: 15px; padding-bottom: 15px;" src="{{ URL::asset('/img/frontend/about-1.png') }}"/>
        </div><!--col-->
    </div><!--row-->
@endsection

@push('pre-footer')
    <div class="row px-0" style="background: #f5f7f6; margin-left: 0px; margin-right: 0px;">
        <div class="container px-0">
            <div class="row" style="background: #f5f7f6; padding-top: 2em; padding-bottom: 2em; margin-left: 0px; margin-right: 0px;">
                <div class="col-xs-12 col-lg-6">
                    <img width="100%" style="padding-top: 15px; padding-bottom: 15px;" src="{{ URL::asset('/img/frontend/about-2.png') }}"/>
                    <h3>Vision</h3>
                    <p>To be the #1 reliable global platform for connecting the 'right' talent to the 'right' poacher.</p>
                </div>
                <div class="col-xs-12 col-lg-6">
                    <img width="100%" style="padding-top: 15px; padding-bottom: 15px;" src="{{ URL::asset('/img/frontend/about-3.png') }}"/>
                    <h3>Mission</h3>
                    <p>To steer poachers away from biased poaching and allow the focus to purely be on talent and best fit for the opportunity.</p>
                </div>
            </div>
        </div>
    </div>
@endpush
