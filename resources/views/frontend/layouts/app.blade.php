<!DOCTYPE html>
@langrtl
    <html lang="{{ app()->getLocale() }}" dir="rtl">
@else
    <html lang="{{ app()->getLocale() }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'YouPoach is the new video poaching system that brings a dynamic and competitive edge to spotting talent.')">
        <meta name="author" content="@yield('meta_author', 'YouPoach')">
        @yield('meta')

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/frontend.css')) }}

        @stack('after-styles')
    </head>
    <body>
        <div id="app">
            @include('includes.partials.logged-in-as')
            @include('frontend.includes.nav')
            <div id="app-wrapper" class="container-fluid px-0">
                @stack('app-toolbar')
                <div id="app-content" class="container mt-4">
                    @include('includes.partials.messages')
                    @yield('content')
                </div><!-- container -->
                @stack('pre-footer')
            </div><!-- container -->

            @include('frontend.includes.footer')
        </div><!-- #app -->

        <!-- Scripts -->
        @stack('before-scripts')

        {{-- Stripe Checkout has issues with Laravel's app.js --}}
        @if (Request::path() != 'premium'))
            {!! script(mix('js/frontend.js')) !!}
        @endif

        @stack('after-scripts')
        @include('cookieConsent::index')
        @include('includes.partials.ga')
    </body>
</html>
