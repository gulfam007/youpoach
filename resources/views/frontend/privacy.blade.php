@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.general.privacy'))

@push('after-styles')
<style>
    #app {
        background-image: url("/img/frontend/youpoach/youpoach-bg-1920-1080.png");
    }
    .highlight {
        color: #f37721 !important;
    }
</style>
@endpush

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h2 class="highlight">1. Your Privacy is our upmost priority</h2>
                    <p>Our mission is to connect the “right” talent to the “right” poacher and vice versa also to drive
                        transformation in the global working environment. However our key priority is to ensure
                        transparency about the data that we collect from you and clearly communicate how it is used
                        or shared.</p>

                    <h2 class="highlight">2. Why have privacy policies?</h2>
                    <p>Our privacy policy describes how we protect and make use of the information you provide
                        YouPoach when you use this site. Under the General Data Protection Regulations 2018
                        we must comply with certain requirements which are designed to
                        ensure that any Data you provide to us is processed with due care and attention.</p>
                    <p><span class="highlight">YouPoach</span> is committed to ensuring that your privacy is protected. So there’s no need to
                        worry! If you are asked to provide certain information when using this website, it will only be
                        used in ways described in this privacy policy.</p>

                    <h2 class="highlight">3. What sort of data do we collect and how do we use the data?</h2>
                    <p>We collect <span class="highlight">personal data</span> when you sign up for an account, create a profile and message or
                        communicate with others. To create an account you provide data including your name, email
                        address and/or mobile number, and a password. Company administrators will need to
                        provide personal data but also their company registration number and company domain
                        email address. We will also get more information for creating your Company Page.</p>
                    <p>If you use our Services for <span class="highlight">financial transactions</span>, we collect information about the
                        purchase or transaction. This includes your payment information, such as your credit or debit
                        card number, other card information, and other account and authentication information
                        including billing, shipping and contact details.</p>
                    <p>If you communicate using our <span class="highlight">messaging feature</span>, we track your conversations. We collect
                        information about you when you send, receive, or engage with messages in connection with
                        <span class="highlight">YouPoach</span>. For example, if you get poached, we track whether you have acted on it and will
                        send you reminders. We also use automatic scanning technology on messages.</p>
                    <p>We collect information about how you use our Services, such as the types of content you
                        view or engage with or the frequency and duration of your activities.</p>
                    <p>Our Services allow you to be found for, <span class="highlight">career</span> opportunities. Your profile can be found by
                        those looking to hire (for a job or a specific task). You can signal that you are interested in
                        changing jobs and share information with <span class="highlight">poachers</span>.</p>
                    <p>We contact you and enable <span class="highlight">communications</span> between members. We offer settings to
                        control what and how often you receive some types of messages. We will contact you
                        through email, notices posted on our websites or apps, messages to your YouPoach inbox,
                        and other ways through our Services, including text messages and push notifications. We
                        will send you messages about the availability of our Services, security, or other service-
                        related issues. We also send messages about how to use our Services, network updates,
                        reminders, job suggestions and promotional messages from us and our partners. You may
                        change your communication preferences at any time.</p>
                    <p>Please be aware that you cannot opt out of receiving service messages from us, including
                        security and legal notices.</p>
                    <p>We do <b><i>not</i></b> require members to include <span class="highlight">sensitive data</span> (e.g. race, ethnicity, political opinions,
                        religious or philosophical beliefs, membership of a trade union, physical or mental health,
                        sexual preference or criminal history) in their <span class="highlight">YouPoach</span> profile. If you choose to input such
                        data to your profile, understand that it is visible to others as noted above.</p>
                    <p>We collect <span class="highlight">device information</span> from or about the computer, phone, or other devices where
                        you install or access our Services, depending on the permissions you’ve granted. We may
                        compare the information we collect from your devices, which helps us provide consistent
                        Services.</p>
                    <p>We collect information in the <span class="highlight">aggregate</span> to provide us with a better understanding of the
                        users of our Website as a group e.g. browsing patterns and preferences. This Aggregate
                        Data does not contain personally identifiable information.</p>
                    <p>For <span class="highlight">marketing</span> purposes we promote our Services to you and others. We use data and
                        content about Members for invitations and communications promoting membership and
                        network growth, engagement and our Services.</p>
                    <p>To develop our services we use data, including public feedback, to conduct relevant
                        <span class="highlight">research</span> and development activities for the further expansion of our Services. So that we
                        can provide you and others with a better, more intuitive and personalized experience and
                        drive membership growth and engagement of our Services. In addition to help connect
                        professionals to each other and to economic opportunity.</p>
                    <p>We are keen to proactively create economic opportunities for our members of the global
                        workforce and to help them be more productive and successful. We use the data available to
                        us to research social, economic and workplace trends such as jobs availability and skills
                        needed for these <span class="highlight">jobs</span> and policies that help bridge the gap in various industries and
                        geographic areas. In some cases, we work with trusted third parties to perform this research,
                        under <span class="highlight">controls</span> that are designed to protect your privacy. We publish or allow others to
                        publish economic insights, presented as aggregated data rather than personal data.</p>
                    <p><span class="highlight">Polls and surveys</span> are conducted by us and others through our Services. You are not
                        obligated to respond to polls or surveys and you have choices about the information you
                        provide. You may opt out of survey invitations.</p>
                    <p>Your data is used for <span class="highlight">customer support</span> activities. We use data to help you and fix
                        problems. We use the data (which can include your communications) needed to investigate,
                        respond to and resolve complaints and Service issues (e.g., bugs).</p>
                    <p><span class="highlight">More you should know.</span> We are always finding new ways to improve our Services, which
                        means that we will be collecting new data to create new ways to use data. We don’t stop!
                        Our Services are dynamic and we often introduce new features, which may require the
                        collection of new information. If we collect significantly different personal data or substantially
                        change how we use your data, we will notify you and may adjust this Privacy Policy to align
                        with the new changes.</p>

                    <h2 class="highlight">4. Why do we collect and process sensitive personal data?</h2>
                    <p>We collect and process Sensitive Personal Data only so far as is necessary and in
                        compliance with all applicable legislation. By using YouPoach and by registering your details
                        with us, you consent to us collecting and processing Sensitive Personal Data supplied by
                        you and disclosing this information to prospective poachers (employers), and clients in
                        connection with the Recruitment Process.</p>

                    <h2 class="highlight">5. Do we pass data to third parties?</h2>
                    <p>Your <span class="highlight">Personal Data may be passed to Third Parties</span> that help us to process Personal
                        Data, to prospective or intended poachers (or third parties assisting them in the recruitment
                        process) or customers for the purpose of recruitment. We may also pass Aggregate Data to
                        carefully selected Third Parties to help us deliver content which is targeted to your
                        preferences.</p>

                    <h2 class="highlight">6. How is this data safeguarded?</h2>
                    <p>Protecting <span class="highlight">your Data is extremely important</span> to us. Access to your Personal Data is only
                        provided to our staff and Third Parties who help us to process data, to prospective poachers
                        or customers in order to help with the Recruitment Process. We have security measures in
                        place to ensure your data is only obtained within the YouPoach boundaries.</p>

                    <h2 class="highlight">7. How do we let you know if our policy changes?</h2>
                    <p>YouPoach may <span class="highlight">change this policy from time to time</span> by updating this page. You should
                        check this page from time to time to ensure that you are happy with any changes. If
                        substantial changes are made, it may be promoted on the Website or through an e-mail
                        notification. But for your information, our privacy policy was last updated in May 2018.</p>

                    <h2 class="highlight">8. What are &#39;cookies&#39; and why do we use them?</h2>
                    <p>&#39;Cookies&#39; are ways of saving a small amount of personal information. <span class="highlight">We use cookies to
                        save information</span> about what type of opportunity you&#39;re looking for. This allows easy access
                        to our site, with recommendations that are relevant to you. If your computer is shared by
                        other people, we advise that you untick the &#39;keep me signed in&#39; checkbox when you sign in
                        to www.youpoach.com This will remove all details from the cookie. If you would like more
                        information about cookies, go to the Website <a style="text-decoration: underline; text-decoration-color: darkorange;" href="http://www.allaboutcookies.org/cookies/"><span class="highlight">all about cookies</span></a>. &#39;Cookies&#39; are ways of saving
                        a small amount of personal information.</p>
                    <p>To find out about all the cookies used on our Website and how to manage them, visit
                        our <span class="highlight">cookies page</span>.</p>

                    <h2 class="highlight">9. How do you remove your name from the database?</h2>
                    <p>If you choose to close your YouPoach account, your personal data will generally stop being
                        visible to others on our Services within 24 hours. We generally delete closed account
                        information within 30 days of account closure, except as noted below.</p>
                    <p>We retain your personal data even after you have closed your account if reasonably
                        necessary to comply with our legal obligations (including law enforcement requests), meet
                        regulatory requirements, resolve disputes, maintain security, prevent fraud and abuse,
                        enforce our User Agreement, or fulfil your request to “unsubscribe” from further messages
                        from us. We will retain de-personalized information after your account has been closed.</p>
                    <p>Information you have shared with others (e.g. through messages) will remain visible after
                        you closed your account or deleted the information from your own profile or mailbox, and we
                        do not control data that other Members copied out of our Services. Your profile may continue
                        to be displayed in the services of others (e.g., search engine results) until they refresh their
                        cache.</p>

                    <h2 class="highlight">10. How do you unsubscribe from future mailings?</h2>
                    <p>If you have an email account you can stop receiving emails by <span class="highlight">updating your contact
                        preferences</span>.</p>

                    <h2 class="highlight">Registered Office</h2>
                    <p>(Address)</p>
                    <p>Company Number (XXXXXX). Registered in England and Wales.</p>

                    <h2 class="highlight">Terminology</h2>
                    <p><span class="highlight">Aggregate Data</span> - this is when all data is collected and processed as a total to enable us to
                        look at such information as demographic and geographic trends, so that we can try to
                        produce a better service in the future. This data is not personally identifiable data.</p>
                    <p><span class="highlight">Data</span> - information which is being processed by equipment operating automatically in
                        response to instructions given for that purpose, is recorded with the intention that it should
                        be processed by means of such equipment, is recorded as part of a relevant filing system or
                        with the intention that it should form part of a relevant filing system or forms part of an
                        accessible record.</p>
                    <p><span class="highlight">Data Protection Act 1998</span> - this is the act of Parliament which set out principles relating to
                        the use of data to ensure that the rights of the person who is the subject of the data are
                        protected under the law.</p>
                    <p><span class="highlight">Personal data</span> - data which relates to a living individual who can be identified from those
                        data, or from those data and other information which is in the possession of, or is likely to
                        come into the possession of, YouPoach.</p>
                    <p><span class="highlight">Privacy policy</span> – this statement describes how we protect and make use of the information
                        you provide YouPoach when you use this site.</p>
                    <p><span class="highlight">Recruitment process</span> - the process of gaining employment and recruiting new staff and the
                        ongoing administrative process involved once a user has gained employment or recruited
                        new staff.</p>
                    <p><span class="highlight">Sensitive personal data</span> - personal data consisting of information as to the racial or ethnic
                        origin of the data subject, political opinions, religious beliefs or other beliefs of a similar
                        nature, membership of a trade union, physical or mental health or condition, sexual
                        preferences, the commission or alleged commission of any offence or any proceedings
                        related to any offence.</p>
                    <p><span class="highlight">Third Parties</span> - these are companies and people other than YouPoach.</p>
                    <p><span class="highlight">Users</span> - people who use our Website and register their details with us for the purposes of
                        recruitment, either to gain employment or for the purposes of recruiting new staff.</p>
                    <p><span class="highlight">Website</span> - YouPoach Online Limited&#39;s website <a style="text-decoration: underline; text-decoration-color: darkorange;" href="https://www.YouPoach.co.uk"><span class="highlight">www.YouPoach.co.uk</span></a> and/or mobile device
                        application.</p>
                    <p><span class="highlight">YouPoach</span> - YouPoach limited. Video Poaching system.</p>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
